# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    if ch == ch.downcase
      str.delete!(ch)
    end
  end

end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  i = str.length/2
  if str.length % 2 != 0
    str[i]
  else
    str[i-1..i]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |ch|
    if VOWELS.include? (ch)
      count += 1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).to_a.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr.each_with_index do |el, i|
    if i < arr.length - 1
      str << el + separator
    else
      str << el
    end
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_arr = []
  str.chars.each_with_index do |ch, i|
    if i % 2 == 0
      weird_arr << ch.downcase
    else
      weird_arr << ch.upcase
    end
  end
  weird_arr.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_e("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  reversed = []
  str.split.each do |word|
    if word.length >= 5
      reversed << word.reverse
    else
      reversed << word
    end
  end
  reversed.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = (1..n).to_a
  i = 0
  while i < arr.length
      if arr[i]%3==0 && arr[i]%15!=0
        arr[i]="fizz"
      elsif arr[i]%5==0 && arr[i]%15!=0
        arr[i]="buzz"
      elsif arr[i]%15==0
        arr[i]="fizzbuzz"
      end
    i=i+1
    end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  i = 0
  arr1 = arr.dup
  while i < arr.length
    arr1.unshift(arr[i])
    i += 1
  end
  arr1[0..arr.length-1]
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num/2).each do |i|
    if num % i == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = (1..num).to_a
  factors.select do |n|
    num % n == 0
  end
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_factors = []
  factors(num).each do |factor|
    prime_factors << factor if prime?(factor)
  end
  prime_factors
end


# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  i=0
  count = 0
  until count == 2 || i == arr.length
    if arr[i]%2==0
      count = count+1
    end
    i=i+1
  end
  if count > 1
    z=arr.select {|x|
    x%2!=0}
  # majority odd
  else
    z=arr.select{ |x|
    x%2==0}
  end
  z[0]
end
